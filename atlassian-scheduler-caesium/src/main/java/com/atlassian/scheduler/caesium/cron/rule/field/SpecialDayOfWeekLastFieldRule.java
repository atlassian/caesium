package com.atlassian.scheduler.caesium.cron.rule.field;

import org.joda.time.LocalDate;

import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.cronToIso;
import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.isoToCron;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Special rule for the last occurrence of the given day-of-week, such as the last Friday every month.
 *
 * @since v0.0.1
 */
public class SpecialDayOfWeekLastFieldRule extends SpecialDayFieldRule {
    private static final long serialVersionUID = 4480059862309727633L;

    private final int isoDayOfWeek;  // 1=Monday

    public SpecialDayOfWeekLastFieldRule(int cronDayOfWeek) {
        super();
        checkArgument(cronDayOfWeek >= 1 && cronDayOfWeek <= 7, "cronDayOfWeek must be in the range [1,7]");

        this.isoDayOfWeek = cronToIso(cronDayOfWeek);
    }

    @Override
    int calculateMatchingDay(final int year, final int month) {
        final LocalDate lastDayOfMonth = new LocalDate(year, month, 1).dayOfMonth().withMaximumValue();
        LocalDate date = lastDayOfMonth.withDayOfWeek(isoDayOfWeek);
        if (date.isAfter(lastDayOfMonth)) {
            // Setting the day of week could move us either forwards or backwards.  If it moved us
            // forwards, then we left the current month and need to back up a week.
            date = date.minusWeeks(1);
        }
        return date.getDayOfMonth();
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        sb.append(isoToCron(isoDayOfWeek)).append('L');
    }
}
