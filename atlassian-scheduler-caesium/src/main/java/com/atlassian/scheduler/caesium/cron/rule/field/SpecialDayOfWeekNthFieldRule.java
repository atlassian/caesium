package com.atlassian.scheduler.caesium.cron.rule.field;

import org.joda.time.LocalDate;

import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.cronToIso;
import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.isoToName;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Rules based on target day of week that uses the {@code #} flag and is therefore guaranteed to match at
 * most a single day in any given month.
 * <p>
 * Note that this does not include the case of {@code L} specified by itself, which just means (any) Saturday.
 * </p>
 *
 * @since v0.0.1
 */
public class SpecialDayOfWeekNthFieldRule extends SpecialDayFieldRule {
    private static final long serialVersionUID = 6051673567197325589L;

    private final int isoDayOfWeek;  // 1=Monday
    private final int nth;  // 1-5

    public SpecialDayOfWeekNthFieldRule(int cronDayOfWeek, int nth) {
        super();
        checkArgument(cronDayOfWeek >= 1 && cronDayOfWeek <= 7, "cronDayOfWeek must be in the range [1,7]");
        checkArgument(nth >= 1 && nth <= 5, "nth must be in the range [1,5]");

        this.isoDayOfWeek = cronToIso(cronDayOfWeek);
        this.nth = nth;
    }

    @Override
    int calculateMatchingDay(final int year, final int month) {
        final LocalDate firstDayOfMonth = new LocalDate(year, month, 1);
        final LocalDate date = calculateNthDayOfWeekFromFirstOfMonth(firstDayOfMonth);

        // If the number requested pushed us past the end of the month, then this month doesn't have a match at all
        final LocalDate lastDayOfMonth = firstDayOfMonth.dayOfMonth().withMaximumValue();
        if (date.isAfter(lastDayOfMonth)) {
            return -1;
        }
        return date.getDayOfMonth();
    }

    private LocalDate calculateNthDayOfWeekFromFirstOfMonth(LocalDate firstDayOfMonth) {
        final LocalDate date = firstDayOfMonth.withDayOfWeek(isoDayOfWeek);
        if (date.isBefore(firstDayOfMonth)) {
            // Setting the day of week could move us either forwards or backwards.  If it moved us
            // backwards, then the count starts from the last week of the previous month, instead.
            return date.plusWeeks(nth);
        }
        return date.plusWeeks(nth - 1);
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        sb.append(isoToName(isoDayOfWeek)).append('#').append(nth);
    }
}
