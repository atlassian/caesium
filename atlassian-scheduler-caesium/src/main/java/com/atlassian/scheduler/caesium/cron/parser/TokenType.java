package com.atlassian.scheduler.caesium.cron.parser;

/**
 * The various kinds of tokens that are recognized by {@link com.atlassian.scheduler.caesium.cron.parser.CronLexer}.
 */
enum TokenType {
    /**
     * A number, such as {@code 0}, {@code 42}, or {@code 2014}.
     */
    NUMBER,

    /**
     * A comma (<code>,</code>) &mdash; used as a separator for multiple values within a field.
     * <p>
     * Example: {@code MON,WED,FRI} yields token types {@code (NAME, COMMA, NAME, COMMA, NAME)}
     * </p>
     */
    COMMA,

    /**
     * A hyphen (<code>-</code>) &mdash; used to identify the minimum and maximum values of a range
     * and also in combination with {@link #FLAG_L} to indicate offsets from the last day of the month.
     * <p>
     * Example: {@code 4-37} yields token types {@code (NUMBER, HYPHEN, NUMBER)}
     * </p>
     */
    HYPHEN,

    /**
     * An asterisk (<code>*</code>) &mdash; used as a wildcard for the full range of the field.
     * <p>
     * Example: <code>&#x2A;/15</code> yields token types {@code (ASTERISK, SLASH, NUMBER)}
     * and would mean the same thing as {@code 0-59/15} or {@code 0,15,30,45} when used in the
     * seconds or minutes field.
     * </p>
     */
    ASTERISK,

    /**
     * A slash (<code>/</code>) &mdash; used to specify the step interval for the range that
     * precedes it, such that the range applies to every nth value, only.
     * When only a single value precedes the slash (as opposed to a range), the maximum
     * legal value for the range is implied as the end value.
     * <p>
     * Example: {@code &#x2A;/20} yields token types {@code (ASTERISK, SLASH, NUMBER)}.
     * In the seconds or minutes field, the implied range is {@code 0-59}, so this would
     * be the same as {@code 0-59/20} or {@code 0,20,40}.
     * </p>
     * <p>
     * Example: {@code 3/7} yields token types {@code (NUMBER, SLASH, NUMBER)}.  In the
     * day-of-month field, the implied end value is {@code 31}, so this is the same as
     * {@code 3-31/7} or {@code 3,10,17,24,31}.  For months with fewer than {@code 31}
     * days, the last value goes unmatched.  The next eligible day after 2014-04-24 would
     * be 2014-05-03.
     * </p>
     */
    SLASH,

    /**
     * A hash mark (<code>#</code>) &mdash; used in the day-of-week field to indicate the nth
     * occurrence of that day of the week in a given month.
     * Months that do not have the requested number of occurrences of that day of the week
     * will not match for that month.
     * <p>
     * Example: {@code WED#3} yields token types {@code (NAME, HASH, NUMBER)} and means the
     * third Wednesday of the month.
     * </p>
     */
    HASH,

    /**
     * The letter ell (<code>L</code>) &mdash; used in the day-of-month or day-of-week field
     * to mean "last".
     * The exact meaning varies depending on context and is probably best illustrated through
     * examples.
     * <p>
     * Example: {@code L} yields token types {@code (FLAG_L)}.  For day-of-month, it means the
     * last day of the month under consideration.  When the month is February, it evaluates to
     * either {@code 28} or {@code 29} depending on whether this is a leap year or not.  When
     * the month is April, it evaluates to {@code 30}.  For the day-of-week field, it just means
     * {@code 7} (Saturday).
     * </p>
     * <p>
     * Example: {@code 5L} yields token types {@code (NUMBER, FLAG_L)}.  This syntax is only valid
     * for the day-of-week field and means the last day of the month for which that is the day of
     * the week.  {@code 5L} means "the last Thursday of the month" and {@code 1L} means "the last
     * Sunday of the month".
     * </p>
     * <p>
     * Example: {@code L-3} yields token types {@code (FLAG_L, HYPHEN, NUMBER)}.  This syntax is
     * only valid for the day-of-month field and means that many days before the last day of the
     * month.  For April, {@code L-3} means {@code 27}.  For February in a leap year, it means
     * {@code 26}.  See the {@link #FLAG_W W} flag for variations on this.
     * </p>
     */
    FLAG_L,

    /**
     * The letter double-u (<code>W</code>) &mdash; used in the day-of-month or day-of-week
     * field to mean "weekday".
     * When used in the day-of-month field, it modifies the value provided to mean the weekday
     * closest to that value but still within the month.  It may only be used with {@link #FLAG_L L}
     * expressions or with a single value.
     * <p>
     * Example: {@code 30W} yields token types {@code (NUMBER, FLAG_W)} and means "the 30th
     * of the month, or the closest weekday to it", meaning the 29th when the 30th is a Saturday
     * or the 31st when the 30th is a Sunday.  If the 30th is a Sunday but there is no 31st of
     * that month, then the closest weekday still in the month would be Friday the 28th.
     * </p>
     * <p>
     * Example: {@code L-3W} yields token types {@code (FLAG_L, HYPHEN, NUMBER, FLAG_W)} and means
     * "the third day from the end of the month, or the closest weekday to it".  For April, this
     * would mean the closest weekday to the 27th.
     * </p>
     */
    FLAG_W,

    /**
     * A sequence of upper-case letters expected to be a three-letter English abbreviation for a month
     * or day-of-week.
     * This type of value is not legal in other fields and must consist of the three initial letters
     * of the value, such as {@code SEP} for September or {@code THU} (note: not {@code THR}!) for
     * Thursday.
     * <p>
     * This token is also returned for any string of (upper-case) letters except {@code L}, {@code W},
     * or {@code LW}, whether a valid three-letter sequence or something else.
     * </p>
     */
    NAME,

    /**
     * A question mark (<code>?</code>) &mdash; must be used for the day-of-month or day-of-week
     * field, but not both, and cannot be combined with any other tokens.
     * <p>
     * In standard cron expressions, the values yielded by these fields would be {@code OR}'d
     * together &mdash; that is, either field matching is good enough to be considered a match.
     * This can be very confusing given that all the other fields are evaluated using {@code AND}
     * instead.  To avoid that odd behaviour, this implementation requires the expression to use
     * specify {@code ?} for one of the two fields to signify that it will not be used and the
     * other has exclusive control of the matching days.  If every day is meant to be valid,
     * specify {@code ?} for one of the fields and {@code *} for the other.  The behaviour is
     * the same either way, but calculations for day-of-week are more expensive than those for
     * day-of-month, so using {@code *} for day-of-month and {@code ?} for day-of-week is
     * slightly better.
     * </p>
     */
    QUESTION_MARK,

    /**
     * Separates fields.
     * May consist of any number of tabs and/or spaces.
     */
    WHITESPACE {
        @Override
        boolean isFieldSeparator() {
            return true;
        }
    },

    /**
     * Returned if more tokens are requested after the cron expression has been fully examined.
     * This is a terminal state; no other token type will ever be returned after this one.
     */
    NOTHING {
        @Override
        boolean isFieldSeparator() {
            return true;
        }
    },

    /**
     * An unexpected character was encountered that is not recognized by the lexer.
     */
    INVALID;

    /**
     * Returns {@code true} for tokens that serve as a boundary between fields; {@code false} for normal tokens.
     *
     * @return {@code true} for tokens that serve as a boundary between fields; {@code false} for normal tokens.
     */
    boolean isFieldSeparator() {
        return false;
    }
}

