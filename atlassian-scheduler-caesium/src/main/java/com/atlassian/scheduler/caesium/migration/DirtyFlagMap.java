package com.atlassian.scheduler.caesium.migration;

import java.io.Serializable;
import java.util.Map;

/**
 * A stub of one of Quartz's classes, for migration purposes.
 *
 * @since v0.0.3
 */
public class DirtyFlagMap implements Serializable {
    private static final long serialVersionUID = 1433884852607126222L;
    private boolean dirty = false;
    private Map<?, ?> map;

    Map<?, ?> unwrap() {
        return map;
    }
}
