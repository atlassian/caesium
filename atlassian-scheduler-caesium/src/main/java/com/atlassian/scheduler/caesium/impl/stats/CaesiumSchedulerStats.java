package com.atlassian.scheduler.caesium.impl.stats;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;

import java.io.Closeable;

/**
 * Stats interface that does not depend on atlassian-stats.
 */ 
public interface CaesiumSchedulerStats extends Closeable {

    String STATS_NAME = "SchedulerStats";

    void jobFlowTakenFromQueue();

    void jobFlowLocalBegin();

    void jobFlowLocalStartedTooEarly();

    void jobFlowLocalPreEnqueue();

    void jobFlowLocalFailedSchedulingNextRun();

    void jobFlowLocalPreLaunch();

    void jobFlowLocalPostLaunch();

    void jobFlowClusteredBegin();

    void jobFlowClusteredSkipNoLongerExists();

    void jobFlowClusteredSkipTooEarly();

    void jobFlowClusteredSkipFailedToClaim();

    void jobFlowClusteredPreEnqueue();

    void jobFlowClusteredPreLaunch();

    void jobFlowClusteredPostLaunch();

    void jobRunnerCompletedSuccessfully(final JobId jobId, long jobRunTimeMillis);

    void jobRunnerFailed(JobId jobId, long jobRunTimeMillis, Throwable t);

    void retryJobScheduled(Throwable throwableOnSchedulingNextRun);

    void retryJobSerializationError(SchedulerServiceException exceptionOnSerialization);

    void retryJobScheduleError(Throwable throwableOnSchedulingRetry);

    void recoveryJobScheduledSuccessfully(Throwable reason);

    void recoveryJobSchedulingFailed(Throwable throwableOnSchedulingRecoveryJob);

    void recoveryJobCompletedSuccessfully(int runNumber);

    void refreshClusteredJobs(int pendingJobCountDifference);

}
