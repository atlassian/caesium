package com.atlassian.scheduler.caesium.utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;

/**
 * This class is used for making assertions on logged event.
 * The implementation has been copied from com.atlassian.jira.imports.project.LogVerifier from jira-tests-unit.
 */
public class LogVerifier implements AutoCloseable {

    private final Logger logger;
    private final Level previousLevel;
    private final TestLogAppender appender;

    public static LogVerifier catchLogMessages(final Class<?> classType) {
        return catchLogMessages(org.apache.log4j.Logger.getLogger(classType));
    }

    public static LogVerifier catchLogMessages(final org.apache.log4j.Logger logger) {
        return new LogVerifier((Logger) LogManager.getLogger(logger.getName()));
    }

    public static LogVerifier catchLogMessages(final org.apache.logging.log4j.Logger logger) {
        return new LogVerifier((Logger) logger);
    }
    public static LogVerifier catchLogMessages() {
        return new LogVerifier((Logger) LogManager.getRootLogger());
    }

    public LogVerifier(final Logger logger) {
        this.logger = logger;
        appender = TestLogAppender.createAppender("LogVerifierAppender-" + UUID.randomUUID());
        appender.start();
        previousLevel = logger.getLevel();

        LoggerContext ctx = (LoggerContext) LogManager.getContext(LogManager.class.getClassLoader(),false);
        Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(logger.getName());
        loggerConfig.setLevel(Level.ALL);
        loggerConfig.addAppender(appender, Level.ALL, null);
        ctx.updateLoggers();
    }

    public void verifyEventLogged(Matcher<LogEvent> logEventMatcher) {
        assertThat(appender.getLog(), hasItem(logEventMatcher));
    }

    public void verifyEventNotLogged(Matcher<LogEvent> logEventMatcher) {
        assertThat(appender.getLog(), not(hasItem(logEventMatcher)));
    }

    @SafeVarargs
    public final void verifyEventsLogged(Matcher<LogEvent>... logEventMatchers) {
        Arrays.stream(logEventMatchers).forEach(this::verifyEventLogged);
    }

    @SafeVarargs
    public final void verifyEventsNotLogged(Matcher<LogEvent>... logEventMatchers) {
        Arrays.stream(logEventMatchers).forEach(this::verifyEventNotLogged);
    }

    public void verifyEventCount(int count) {
        assertThat(appender.getLog(), hasSize(count));

    }

    public void verifyNoEventsLogged() {
        verifyEventCount(0);
    }

    @Override
    public void close() throws Exception {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(LogManager.class.getClassLoader(),false);
        Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(logger.getName());
        loggerConfig.setLevel(previousLevel);
        loggerConfig.removeAppender(appender.getName());
        ctx.updateLoggers();
    }

    public static Matcher<LogEvent> level(final Matcher<? super Level> levelMatcher) {
        return new LogEventTypeMatcher(levelMatcher);
    }

    public static Matcher<LogEvent> exception(final Matcher<? super Throwable> subMatcher) {
        return new LogEventExceptionMatcher(subMatcher);
    }

    public static Matcher<LogEvent> renderedMessage(final Matcher<? super String> subMatcher) {
        return new LogEventRenderedMessageMatcher(subMatcher);
    }

    static class LogEventTypeMatcher extends FeatureMatcher<LogEvent, Level> {
        public LogEventTypeMatcher(final Matcher<? super Level> subMatcher) {
            super(subMatcher, "LogEvent level:", "Level");
        }

        @Override
        protected Level featureValueOf(final LogEvent actual) {
            return actual.getLevel();
        }
    }

    static class LogEventExceptionMatcher extends FeatureMatcher<LogEvent, Throwable> {
        public LogEventExceptionMatcher(final Matcher<? super Throwable> subMatcher) {
            super(subMatcher, "LogEvent throwable:", "log throwable");
        }

        @Override
        protected Throwable featureValueOf(final LogEvent actual) {
            return actual.getThrown();
        }
    }

    static class LogEventRenderedMessageMatcher extends FeatureMatcher<LogEvent, String> {
        public LogEventRenderedMessageMatcher(final Matcher<? super String> subMatcher) {
            super(subMatcher, "LogEvent message:", "log message");
        }

        @Override
        protected String featureValueOf(final LogEvent actual) {
            return actual.getMessage().getFormattedMessage();
        }
    }
}
