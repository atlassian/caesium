package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionDayOfMonthTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionDayOfMonthTest extends CronExpressionDayOfMonthTest {
    public CaesiumCronExpressionDayOfMonthTest() {
        super(new CaesiumCronFactory());
    }
}
