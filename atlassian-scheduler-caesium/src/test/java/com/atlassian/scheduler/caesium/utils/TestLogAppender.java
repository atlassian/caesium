package com.atlassian.scheduler.caesium.utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;

import java.util.LinkedList;
import java.util.List;

/**
 * A log appender to be able to check if certain messages have been logged.
 */
class TestLogAppender extends AbstractAppender {
    private final List<LogEvent> log = new LinkedList<>();

    public static TestLogAppender createAppender(final String name) {
        return new TestLogAppender(name, null);
    }

    public TestLogAppender(String name, Filter filter) {
        super(name, filter, null, true, Property.EMPTY_ARRAY);
        this.start();
    }

    public List<LogEvent> getLog() {
        return log;
    }

    public boolean contains(String msg, Level level) {
        for (LogEvent logEntry : log) {
            if (logEntry.getLevel().equals(level) && logEntry.getMessage().toString().contains(msg)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void append(LogEvent event) {
        log.add(event.toImmutable());
    }
}
